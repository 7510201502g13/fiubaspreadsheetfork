package ar.fiuba.tdd.tp1;

import java.io.IOException;

public class InitShell {

    private static ShellInterpreter shellInt = new ShellInterpreter();
    
    public static void main(String[] args) {
        try {
            shellInt.shell();
        } catch (IOException e) {
            System.out.println("Problemas con el sistema de entrada.");
        }
    }
}
