package ar.fiuba.tdd.tp1;

public class Matrix<T> {
	int nRow = 0;
	int nCol = 0;

	@SuppressWarnings("unchecked")
	public Matrix(int n, int m) {
		nRow = n;
		nCol = m;
		mtx = (T[][]) (new Object[n + 10][m + 10]);

	}

	public int getNumRow() {
		return nRow;
	}

	public int getNumCol() {
		return nCol;
	}

	private T[][] mtx;

	public void set(int i, int j, T value) {
		mtx[i][j] = value;
	}

	public T get(int i, int j) {
		return mtx[i][j];
	}

}
