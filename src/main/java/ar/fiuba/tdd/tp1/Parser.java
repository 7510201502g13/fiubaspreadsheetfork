/**
 * class: Parser
 * Purpose: Turn strings into derivative classes of Expression recursively.
 */

package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.ExpressionsFactory.Function;
import ar.fiuba.tdd.tp1.expressions.Expression;

import java.util.ArrayList;
import java.util.List;

public class Parser {

    private static String REGEXPSUM = ".*\\+.*";
    private static String REGEXPSUBTRACT = ".*-.*";
    private static String REGEXPVALUE = "-?[0-9]+(\\.[0-9]+)?";
    private static String REGEXPCELL = "([a-zA-Z0-9]+\\!)?[a-zA-Z]+[0-9]+";
    private static String REGEXPFUNCTION = "[a-zA-Z]+\\(.*\\)";
    private static String REGEXPCONCAT = "CONCAT\\(.*,.*\\)";
    private static String REGEXPMAX = "MAX\\(.*\\)";
    private static String REGEXPMIN = "MIN\\(.*\\)";
    private static String REGEXPAVG = "AVERAGE\\(.*\\)";
    private static String SUM = "\\+";
    private static String SUBTRACT = "-";

    private SpreadSheet spreadSheet;
    private ExpressionsFactory factory;;

    public Parser(SpreadSheet spreadSheet) {
        this.spreadSheet = spreadSheet;
        this.factory = new ExpressionsFactory(spreadSheet);
    }

    private void concatOperands(String[] resultSplit, List<String> operands) {
        operands.add(resultSplit[0]);
        operands.add(resultSplit[1]);
        for (int i = 2; i < resultSplit.length; i++) {
            operands.set(1, operands.get(1).concat("+"));
            operands.set(1, operands.get(1).concat(resultSplit[i]));
        }
    }

    private void correctNegative(String[] resultSplit) {
        if (resultSplit[0].isEmpty()) {
            resultSplit[0] = "0";
        }
    }

    private Expression parseFunction(String formula) {
        Expression result = null;

        if (formula.matches(REGEXPCONCAT)) {
            result = parseConcat(formula);

        } else if (formula.matches(REGEXPMAX) || formula.matches(REGEXPMIN)
                || formula.matches(REGEXPAVG)) {
            result = parseMaxMinAvg(formula);

        } else {
            throw new InvalidFunctionException();
        }

        return result;
    }

    private Expression parseConcat(String formula) {
        Expression result;
        String[] operands = resolveOperandsConcat(formula);
        result = factory.newConcat(parseFormula(operands[0].trim()),
                parseFormula(operands[1].trim()));
        return result;
    }

    private String[] resolveOperandsConcat(String formula) {
        String[] ops = new String[2];
        ops[0] = formula.substring(formula.indexOf('(') + 1,
                formula.indexOf(','));
        ops[1] = formula.substring(formula.indexOf(',') + 1,
                formula.lastIndexOf(')'));
        return ops;
    }

    private Expression parseMaxMinAvg(String formula) {
        Expression result = null;
        String range = getRange(formula);

        if (formula.matches(REGEXPMAX)) {
            result = factory.newFunctionWithRange(range, Function.MAX);

        } else if (formula.matches(REGEXPMIN)) {
            result = factory.newFunctionWithRange(range, Function.MIN);

        } else if (formula.matches(REGEXPAVG)) {
            result = factory.newFunctionWithRange(range, Function.AVG);
        }

        return result;
    }

    private String getRange(String formula) {
        return formula
                .substring(formula.indexOf('(') + 1, formula.indexOf(')'));
    }

    private Expression parseSum(String formula) {

        String[] aux = formula.split(SUM);
        List<String> operands = new ArrayList<String>();

        concatOperands(aux, operands);
        Evaluable op1 = this.parseFormula(operands.get(0).trim());
        Evaluable op2 = this.parseFormula(operands.get(1).trim());
        return (factory.newArithmeticExpression(op1, op2, '+'));

    }

    private Expression parseSubtract(String formula) {

        String[] aux = formula.split(SUBTRACT);
        List<String> operands = new ArrayList<String>();
        correctNegative(aux);
        concatOperands(aux, operands);
        Evaluable op1 = this.parseFormula(operands.get(0).trim());
        Evaluable op2 = this.parseFormula(operands.get(1).trim());
        return (factory.newArithmeticExpression(op1, op2, '-'));
    }

    private Expression parseCell(String formula) {
        String[] cellRef = formula.split("\\!");
        Expression result;
        if (cellRef.length == 1) {
            result = this.spreadSheet.getDefaultWS()
                    .getCellByName(cellRef[0].trim()).getExpression();
        } else {
            result = this.spreadSheet.getWorksheetByName(cellRef[0].trim())
                    .getCellByName(cellRef[1].trim()).getExpression();
        }
        return result;
    }

    private Expression parseValue(String literal) {
        return (factory.newNumeric(Double.parseDouble(literal.trim())));
    }

    private Expression parseString(String literal) {
        return (factory.newString(literal));
    }

    private String deleteEqualsSymbol(String formula) {
        if (formula.matches("^=.*")) {
            String[] aux = formula.split("=");
            return aux[1];
        }
        return formula;
    }

    private boolean isFormula(String formula) {
        if (formula.matches("^=.*")) {
            return true;
        }
        return false;
    }

    private Expression parseFormula(String formula) {
        Expression result;

        if (formula.matches(REGEXPSUM)) {
            result = parseSum(formula);

        } else if (formula.matches(REGEXPSUBTRACT)) {
            result = parseSubtract(formula);

        } else if (formula.matches(REGEXPCELL)) {
            result = parseCell(formula);

        } else if (formula.matches(REGEXPFUNCTION)) {
            result = parseFunction(formula);

        } else if (formula.matches(REGEXPVALUE)) {
            result = parseValue(formula.trim());

        } else {
            result = parseString(formula);
        }

        return result;
    }

    private Expression parseLiteral(String literal) {
        Expression result;

        if (literal.matches(REGEXPVALUE)) {
            result = parseValue(literal);
        } else {
            result = parseString(literal);
        }
        return result;
    }

    public Expression parse(String formula) {
        Expression result;

        if (isFormula(formula)) {
            formula = deleteEqualsSymbol(formula);
            result = parseFormula(formula);
        } else {
            result = parseLiteral(formula);
        }
        return result;
    }
}
