package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.expressions.Expression;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = Cell.class, name = "Cell"), @Type(value = Expression.class, name = "Expression"), })
public interface Evaluable {
    public Expression evaluate();
}
