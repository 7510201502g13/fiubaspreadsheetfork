package ar.fiuba.tdd.tp1.expressions.formateable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

import ar.fiuba.tdd.tp1.expressions.Expression;
import ar.fiuba.tdd.tp1.format.Format;
import ar.fiuba.tdd.tp1.format.Formatter;
@SuppressWarnings("CPD-START")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @Type(value = StringValueExpression.class, name = "StringValueExpression"),
        @Type(value = DateValueExpression.class, name = "DateValueExpression"),
        @Type(value = NumericValueExpression.class, name = "NumericValueExpression") })
public abstract class Formateable extends Expression {

    protected Format format;

    public abstract String applyFormat();

    public void setFormat(Format formato) {
        this.format = formato;
    }

	
}
