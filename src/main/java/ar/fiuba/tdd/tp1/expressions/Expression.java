package ar.fiuba.tdd.tp1.expressions;

import ar.fiuba.tdd.tp1.Evaluable;
import ar.fiuba.tdd.tp1.expressions.formateable.Formateable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
@SuppressWarnings("CPD-START")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = DoubleExpression.class, name = "DoubleExpression"),
		@Type(value = Formateable.class, name = "Formateable") })

public abstract class Expression implements Evaluable {
	public abstract Expression evaluate();

}
