package ar.fiuba.tdd.tp1.expressions.formateable;

import ar.fiuba.tdd.tp1.format.Formatter;
import ar.fiuba.tdd.tp1.format.string.FormatStringDefault;

public class StringValueExpression extends Formateable {

    private String valor;

    public StringValueExpression(String value) {
        this.valor = value;
        this.format = new FormatStringDefault();
    }

    @Override
    public StringValueExpression evaluate() {
        return (this);
    }

    public String getValor() {
        return valor;
    }

    public String getString(Formatter format) {

        return Formatter.format(this);
    }

    @Override
    public String applyFormat() {
        return this.format.apply(this);

    }

    @Override
	public String toString() {
		return Formatter.format(this);
	}

}
