package ar.fiuba.tdd.tp1.expressions;

import ar.fiuba.tdd.tp1.Evaluable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = ArithmeticExpression.class, name = "ArithmeticExpression") })
abstract class DoubleExpression extends Expression {

    protected Evaluable ev1;
    protected Evaluable ev2;

    public DoubleExpression() {
        super();
    }

    public DoubleExpression(Evaluable ev1, Evaluable ev2) {
        this.ev1 = ev1;
        this.ev2 = ev2;
    }

    public Evaluable getEv1() {
        return ev1;
    }

    public void setEv1(Evaluable ev1) {
        this.ev1 = ev1;
    }

    public Evaluable getEv2() {
        return ev2;
    }

    public void setEv2(Evaluable ev2) {
        this.ev2 = ev2;
    }
}