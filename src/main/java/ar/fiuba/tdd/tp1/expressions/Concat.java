package ar.fiuba.tdd.tp1.expressions;

import ar.fiuba.tdd.tp1.BadFormulaException;
import ar.fiuba.tdd.tp1.Evaluable;
import ar.fiuba.tdd.tp1.expressions.formateable.StringValueExpression;

public class Concat extends Expression {

    private Evaluable ev1;
    private Evaluable ev2;

    public Concat(Evaluable ev1, Evaluable ev2) {
        this.ev1 = ev1;
        this.ev2 = ev2;
    }

    @Override
    public Expression evaluate() {
        try {
            String concatString = ((StringValueExpression) ev1.evaluate())
                    .getValor()
                    .concat(((StringValueExpression) ev2.evaluate()).getValor());
            StringValueExpression stringValue = new StringValueExpression(
                    concatString);
            return stringValue;
        } catch (Exception e) {
            throw new BadFormulaException();
        }
    }
}
