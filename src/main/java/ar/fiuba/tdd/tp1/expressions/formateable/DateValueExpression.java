package ar.fiuba.tdd.tp1.expressions.formateable;

import java.time.LocalDate;

import ar.fiuba.tdd.tp1.format.Formatter;
import ar.fiuba.tdd.tp1.format.date.FormatDateDMA;

public class DateValueExpression extends Formateable {

    private LocalDate valor;

    public DateValueExpression(LocalDate value) {
        this.valor = value;
        this.format = new FormatDateDMA();
    }

    public  DateValueExpression(int dia, int mes, int anio) {
    	this.valor= LocalDate.of(anio, mes, dia );
    }
    
    @Override
    public DateValueExpression evaluate() {
        return (this);
    }

    public LocalDate getValor() {
        return this.valor;
    }

    @Override
    public String applyFormat() {
        return this.format.apply(this);
    }

    @Override
	public String toString() {
		return Formatter.format(this);
	}

}
