package ar.fiuba.tdd.tp1.expressions;

import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tdd.tp1.Evaluable;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;

public class Average extends Expression {

    private List<Evaluable> cells = new ArrayList<Evaluable>();

    public Average(List<Evaluable> cells) {
        this.cells = cells;
    }

    @Override
    public Expression evaluate() {
        Double avg = 0.0;

        for (Evaluable ev : cells) {
            avg += ((NumericValueExpression) ev.evaluate()).getValor();
        }
        avg = avg / cells.size();
        return (new NumericValueExpression(avg));
    }
}
