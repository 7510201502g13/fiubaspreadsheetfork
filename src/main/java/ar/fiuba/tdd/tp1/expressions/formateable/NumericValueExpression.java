package ar.fiuba.tdd.tp1.expressions.formateable;

import ar.fiuba.tdd.tp1.format.Formatter;
import ar.fiuba.tdd.tp1.format.numeric.FormatNumericConDecimales;
import ar.fiuba.tdd.tp1.format.numeric.FormatNumericSinDecimales;

public class NumericValueExpression extends Formateable {

    private Double valor;

    public NumericValueExpression() {
        super();

    }

    public NumericValueExpression(double value) {
        this.valor = value;
    	this.format = new FormatNumericSinDecimales();
    }

    @Override
    public NumericValueExpression evaluate() {
        return (this);
    }

    public double getValor() {
        return valor;
    }

    @Override
	public String toString() {
		return Formatter.format(this);
	}

    @Override
    public String applyFormat() {
        return this.format.apply(this);

    }

}
