/**
 * class: AddExpression
 * Purpose: represents operation ADD. Receive two operands of type Evaluable
 */

package ar.fiuba.tdd.tp1.expressions;

import ar.fiuba.tdd.tp1.Evaluable;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;

public class ArithmeticExpression extends DoubleExpression {

    private char operation;

    public ArithmeticExpression() {
        super();
    }

    public ArithmeticExpression(Evaluable ev1, Evaluable ev2, char op) {
        super(ev1, ev2);
        this.operation = op;
    }

    public char getOperation() {
        return operation;
    }

    public void setOperation(char operation) {
        this.operation = operation;
    }

    public static ArithmeticExpression create(Evaluable ev1, Evaluable ev2,
            char operation) {
        return new ArithmeticExpression(ev1, ev2, operation);
    }

    @Override
    public NumericValueExpression evaluate() {
        double resultado = 0.0;
        if (operation == '+') {
            resultado = ((NumericValueExpression) this.ev1.evaluate())
                    .getValor()
                    + ((NumericValueExpression) this.ev2.evaluate()).getValor();
        } else if (operation == '-') {
            resultado = ((NumericValueExpression) this.ev1.evaluate())
                    .getValor()
                    - ((NumericValueExpression) this.ev2.evaluate()).getValor();
        } else {
            throw new InvalidOperationException();
        }

        return (new NumericValueExpression(resultado));
    }
}