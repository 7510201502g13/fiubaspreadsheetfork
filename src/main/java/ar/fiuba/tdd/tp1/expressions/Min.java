package ar.fiuba.tdd.tp1.expressions;

import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tdd.tp1.Evaluable;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;

public class Min extends Expression {

    private List<Evaluable> cells = new ArrayList<Evaluable>();

    public Min(List<Evaluable> cells) {
        this.cells = cells;
    }

    @Override
    public Expression evaluate() {
        Double min = ((NumericValueExpression) cells.get(0).evaluate())
                .getValor();
        ;

        for (int i = 1; i < cells.size(); i++) {
            if (((NumericValueExpression) cells.get(i).evaluate()).getValor() < min) {
                min = ((NumericValueExpression) cells.get(i).evaluate())
                        .getValor();
            }
        }
        return (new NumericValueExpression(min));
    }
}