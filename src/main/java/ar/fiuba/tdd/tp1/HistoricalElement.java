package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.expressions.Expression;

public class HistoricalElement {

    private Expression expression;
    String nameCell;
    String nameWorksheet;

    public HistoricalElement(String nameWorksheet, String nameCell, Cell cell) {
        this.nameWorksheet = nameWorksheet;
        this.expression = cell.expression;
        this.nameCell = nameCell;
    }

    public void setExpression(Expression valor) {
        this.expression = valor;
    }

    public Expression getExpression() {
        return expression;
    }

    public String getNameCell() {
        return nameCell;
    }

    public void setNameCell(String name) {
        this.nameCell = name;
    }

    public String getNameWorksheet() {
        return nameWorksheet;
    }

    public void setNameWorksheet(String nameWorksheet) {
        this.nameWorksheet = nameWorksheet;
    }

    public String getID() {
        return getNameWorksheet() + getNameCell();
    }
}
