package ar.fiuba.tdd.tp1.format.numeric;

import java.text.DecimalFormat;

import ar.fiuba.tdd.tp1.expressions.formateable.Formateable;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.format.FormatNumeric;
import ar.fiuba.tdd.tp1.format.money.FormatMoneyConDecimales;

public class FormatNumericConDecimales extends FormatNumeric {

    private int numeroDecimales;

    public FormatNumericConDecimales(int decimales) {
        this.numeroDecimales = decimales;

    }

    @Override
    public String apply(Formateable formateable) {
        NumericValueExpression expression = (NumericValueExpression) formateable;

        StringBuffer sb = new StringBuffer();
        sb.append("#.");
        for (int i = 0; i < numeroDecimales; i++) {
            sb.append(0);
        }
        DecimalFormat decimalFormat = new DecimalFormat(sb.toString());
        return decimalFormat.format(expression.getValor());
    }
}
