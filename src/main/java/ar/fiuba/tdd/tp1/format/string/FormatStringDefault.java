package ar.fiuba.tdd.tp1.format.string;

import ar.fiuba.tdd.tp1.expressions.formateable.Formateable;
import ar.fiuba.tdd.tp1.expressions.formateable.StringValueExpression;
import ar.fiuba.tdd.tp1.format.FormatString;

public class FormatStringDefault extends FormatString{

    public String apply(Formateable formateable) {

        StringValueExpression expression = (StringValueExpression) formateable;

        return expression.getValor();
    }
}
