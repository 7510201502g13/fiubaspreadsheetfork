package ar.fiuba.tdd.tp1.format.date;

import java.time.format.DateTimeFormatter;

import ar.fiuba.tdd.tp1.expressions.formateable.DateValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.Formateable;
import ar.fiuba.tdd.tp1.format.FormatDate;

public class FormatDateAMD extends FormatDate {

    @Override
    public String apply(Formateable formateable) {
        DateValueExpression expression = (DateValueExpression) formateable;

        return expression.getValor().format(DateTimeFormatter.ISO_DATE);
    }

}
