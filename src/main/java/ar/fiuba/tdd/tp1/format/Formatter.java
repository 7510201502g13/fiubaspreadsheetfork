package ar.fiuba.tdd.tp1.format;

import ar.fiuba.tdd.tp1.expressions.formateable.DateValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.StringValueExpression;

public class Formatter {

    public static final String format(StringValueExpression expression) {

        return expression.applyFormat();

    }

    public static final String format(DateValueExpression expression) {

        return expression.applyFormat();

    }

    public static final String format(NumericValueExpression expression) {

        return expression.applyFormat();

    }

}
