package ar.fiuba.tdd.tp1.format.numeric;

import java.text.DecimalFormat;

import ar.fiuba.tdd.tp1.expressions.formateable.Formateable;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.format.FormatNumeric;

public class FormatNumericSinDecimales extends FormatNumeric {


    public FormatNumericSinDecimales() {
   

    }

    @Override
    public String apply(Formateable formateable) {
        NumericValueExpression expression = (NumericValueExpression) formateable;

        StringBuffer sb = new StringBuffer();
        sb.append("#");
        DecimalFormat decimalFormat = new DecimalFormat(sb.toString());
        return decimalFormat.format(expression.getValor());
    }
}
