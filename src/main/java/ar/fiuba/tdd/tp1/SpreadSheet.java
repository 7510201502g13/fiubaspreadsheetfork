package ar.fiuba.tdd.tp1;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SpreadSheet {

    private String name;
    private List<WorkSheet> worksheets;
    private WorkSheet defaultWS;
    private Historical historical;

    public SpreadSheet() {
        super();
    }

    private SpreadSheet(String name) {
    	this.historical= new Historical();
        worksheets = new ArrayList<WorkSheet>();
        this.name = name;
    }

    public List<WorkSheet> getWorksheets() {
        return worksheets;
    }

    public void setWorksheets(List<WorkSheet> worksheets) {
        this.worksheets = worksheets;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static SpreadSheet createSpreadsheet(String name) {
        return new SpreadSheet(name);
    }

    public void addExternalWorkSheet(WorkSheet workSheet) {
        if (worksheets.contains(workSheet)) {
            throw new SheetAlreadyExistsException();
        } else {
            worksheets.add(workSheet);
        }
    }

    public WorkSheet addNewWorksheet(String name) {

        WorkSheet work = WorkSheet.createWorkSheet(name);
        if (worksheets.contains(work)) {
            throw new SheetAlreadyExistsException();
        } else {
            worksheets.add(work);
        }
        return work;
    }

    @JsonIgnore
    public WorkSheet getWorksheetByName(String name) {
        WorkSheet ws = WorkSheet.createWorkSheet(name);
        if (worksheets.contains(ws)) {
            return worksheets.get(worksheets.indexOf(ws));
        } else {
            throw new SheetDoesNotExistException();
        }
    }

    public List<WorkSheet> getWorkSheets() {
        return this.worksheets;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int hashCode() {
    	int result = 1;
        final int prime = 31;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return (prime * result + ((worksheets == null) ? 0 : worksheets.hashCode()));
    }

    @JsonIgnore
    public WorkSheet getDefaultWS() {
        return this.defaultWS;
    }

    @JsonIgnore
    public void setDefaultWS(WorkSheet ws) {
        this.defaultWS = ws;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SpreadSheet other = (SpreadSheet) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @JsonIgnore
	public Historical getHistorical() {
		return historical;
	}

    @JsonIgnore
	public void setHistorical(Historical historical) {
		this.historical = historical;
	}
    
}