package ar.fiuba.tdd.tp1;

import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tdd.tp1.expressions.ArithmeticExpression;
import ar.fiuba.tdd.tp1.expressions.Average;
import ar.fiuba.tdd.tp1.expressions.Concat;
import ar.fiuba.tdd.tp1.expressions.Expression;
import ar.fiuba.tdd.tp1.expressions.Max;
import ar.fiuba.tdd.tp1.expressions.Min;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.StringValueExpression;

public class ExpressionsFactory {

    public enum Function {
        MAX, MIN, AVG
    };

    private SpreadSheet spreadSheet;

    public ExpressionsFactory(SpreadSheet spreadSheet) {
        this.spreadSheet = spreadSheet;
    }

    public List<Evaluable> newRange(String range) {
        validateValidRange(range);

        List<Evaluable> cellsInRange = new ArrayList<Evaluable>();
        String[] borderCells = range.split(":");
        String[] lowerCell = borderCells[0].split("!");
        String[] higherCell = borderCells[1].split("!");
        WorkSheet ws;
        String cellInf;
        String cellSup;
        if (lowerCell.length > 1) {
        	ws = this.spreadSheet.getWorksheetByName(lowerCell[0]);
        	cellInf = lowerCell[1];
        	cellSup = higherCell[1];
        } else {
        	ws = this.spreadSheet.getDefaultWS();
        	cellInf = lowerCell[0];
        	cellSup = higherCell[0];
        }

        for (String cellName : ws.getCellNames()) {
            if (isIntoRange(cellName, cellInf, cellSup)) {
                cellsInRange.add(ws.getCellByName(cellName));
            }
        }
        return cellsInRange;
    }

    private void validateValidRange(String range) {

        String[] borders = range.split(":");
        String[] lowerCell = borders[0].split("!");
        String[] higherCell = borders[1].split("!");

        if (lowerCell.length > 1) {
        	if (!lowerCell[0].equals(higherCell[0])) {
                throw new InvalidRangeException();
            }
        }
        
        if (!range.matches(".+:.+")) {
            throw new InvalidRangeException();
        }
    }

    private boolean isIntoRange(String cellName, String lowerCell,
            String higherCell) {
        if (cellName.compareToIgnoreCase(lowerCell) >= 0
                && cellName.compareToIgnoreCase(higherCell) <= 0) {
            return true;
        }
        return false;
    }

    public Expression newArithmeticExpression(Evaluable ev1, Evaluable ev2,
            char operator) {

        return new ArithmeticExpression(ev1, ev2, operator);
    }

    public Expression newFunctionWithRange(String range, Function func) {

        switch (func) {
        case MAX:
            return new Max(newRange(range));
        case MIN:
            return new Min(newRange(range));
        case AVG:
            return new Average(newRange(range));
        default:
            throw new InvalidFunctionException();
        }
    }

    public Expression newConcat(Expression op1, Expression op2) {
        return new Concat(op1, op2);
    }

    public Expression newNumeric(Double value) {
        return new NumericValueExpression(value);
    }

    public Expression newString(String literal) {
        return new StringValueExpression(literal);
    }
}
