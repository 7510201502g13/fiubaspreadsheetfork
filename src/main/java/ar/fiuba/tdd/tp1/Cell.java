/**
 * class: Cell
 * Purpose: Represents a cell in a worksheet. It's able to manage simple values or expressions.
 */

package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.expressions.Expression;

public class Cell implements Evaluable {

    Expression expression;

    public Cell() {
        super();
    }

    public static Cell createCell() {
        return new Cell();
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expr) {
        this.expression = expr;
    }

    public Expression evaluate() {
        return expression.evaluate();

    }
}
