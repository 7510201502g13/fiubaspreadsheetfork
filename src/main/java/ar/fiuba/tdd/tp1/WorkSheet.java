package ar.fiuba.tdd.tp1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WorkSheet {

    private String name;
    private HashMap<String, Cell> hashCell;

    private WorkSheet(String name) {
        this.hashCell = new HashMap<String, Cell>();
        this.name = name;
    }

    public WorkSheet() {
    	
	}

	public HashMap<String, Cell> getHashCell() {
        return hashCell;
    }
	
	@JsonIgnore
	public List<String> getCellNames() {
	    List<String> cells = new ArrayList<String>();
	    Set<String> setCells = this.hashCell.keySet();
	    
	    for (String cell: setCells) {
	        cells.add(cell);
	    }
	    return cells;
    }

    public void setHashCell(HashMap<String, Cell> hashCell) {
        this.hashCell = hashCell;
    }

    public void setName(String name) {
        this.name = name;
    }

    private boolean cellExists(String name) {
        if (hashCell.containsKey(name)) {
            return true;
        } else {
            return false;
        }
    }

    public static WorkSheet createWorkSheet(String name) {
        return new WorkSheet(name);
    }

    public Cell addNewCell(String name) {

        Cell cell = Cell.createCell();
        if (!cellExists(name)) {
            hashCell.put(name, cell);
        } else {
            throw new CellAlreadyExistException();
        }
        return cell;
    }

    @JsonIgnore
    public Cell getCellByName(String name) {
        if (cellExists(name)) {
            return hashCell.get(name);
        } else {
            return addNewCell(name);
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        WorkSheet other = (WorkSheet) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }
}
