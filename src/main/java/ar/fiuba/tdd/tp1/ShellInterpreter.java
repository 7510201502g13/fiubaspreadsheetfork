package ar.fiuba.tdd.tp1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tdd.tp1.expressions.formateable.Formateable;
import ar.fiuba.tdd.tp1.format.date.FormatDateAMD;
import ar.fiuba.tdd.tp1.format.date.FormatDateDMA;
import ar.fiuba.tdd.tp1.format.date.FormatDateMDA;
import ar.fiuba.tdd.tp1.format.money.FormatMoneyConDecimales;
import ar.fiuba.tdd.tp1.format.numeric.FormatNumericConDecimales;


public class ShellInterpreter {
    
    private SpreadSheet spreadSheet;
    private Parser parser;
    private Exporter exporter;
    
    public void init() {
        this.spreadSheet = SpreadSheet.createSpreadsheet("book1");
        WorkSheet ws =this.spreadSheet.addNewWorksheet("default");
        this.spreadSheet.setDefaultWS(ws);
        this.parser = new Parser(this.spreadSheet);
        this.exporter = new Exporter();
    }
    
    public void shell() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));  
        init();
        prompt();
        String line = new String (br.readLine());
        String[] args = line.split("\\s+");
        
        while (!args[0].trim().equals("exit")) {
            
            switch (args[0].trim()) {
            case "newws":
                addWorkSheet(args);
                break;
            case "set": 
                setCell(args);
                break;
            case "format": 
                formatCell(args);
                break;
            case "redo": 
                redo();
                break;
            case "undo": 
                undo();
                break;
            case "export": 
                export(args);
                break;
            case "import": 
                imports(args);
                break;
            default: 
                invalidCommand(); 
            }
            prompt();
            line = br.readLine();
            args = line.split("\\s+");
        }
        System.out.println("Bye!");
    }
    
    private void prompt() {
        System.out.print("spreadsheet@" + System.getProperty("user.name") + "$: ");
    }

    private void invalidCommand() {
        System.out.println("Comando invalido, por favor ingrese un comando valido o ingrese \"help\" para"
                + "obtener ayuda");
    }
    
    private void invalidParams() {
        System.out.println("Parametros invalido, por favor ingreselos nuevamente o chequee la "
                + "sintaxis del comando -> help nombre_comando");
    }

    /**
     * 
     * @param args: args[2] -> WorkSheet name
     */
    private void addWorkSheet(String[] args) {
        if (args.length == 2) {
            try {
                this.spreadSheet.addNewWorksheet(args[1].trim());
            } catch (SheetAlreadyExistsException e) {
                System.out.println("El worksheet que intenta crear ya existe");
            }
        } else {
            invalidParams();
        }
    }

    private void setCell(String[] args) {
        if (args.length == 4) {
        	String nameWorksheet= args[1];
        	String nameCell= args[2];
        	WorkSheet ws = this.spreadSheet.getWorksheetByName(nameWorksheet);        	
        	Cell cell= ws.getCellByName(nameCell);
        	try {
        		cell.setExpression(parser.parse(args[3]));
        		this.spreadSheet.getHistorical().addCell(new HistoricalElement(nameWorksheet, nameCell, cell));;
                drawSheet(nameWorksheet);
        	} catch (Exception e) {
        		invalidParams();
        	}
        } else {
            invalidParams();
        }
    }    
    
    private void drawSheet(String nameWorksheet) {
    	List<String> cellNames = new ArrayList<String>();
    	WorkSheet ws = this.spreadSheet.getWorksheetByName(nameWorksheet); 
		cellNames = ws.getCellNames();
		System.out.println("Sheet: " + ws.getName());
    	for (String cellName: cellNames) {
    		Cell cell = ws.getCellByName(cellName);
    		System.out.println(cellName + " -> " + cell.evaluate().toString());
    	}
	}

	private void formatCell(String[] args) {
		if (args.length == 4) {
        	String nameWorksheet= args[1];
        	String nameCell= args[2];
        	String nameFormat = args[3];
        	WorkSheet ws = this.spreadSheet.getWorksheetByName(nameWorksheet);        	
        	Cell cell= ws.getCellByName(nameCell);
        	try {
        		setFormat(cell,nameFormat);
        	} catch (Exception e) {
        		invalidParams();
        	}
            drawSheet(nameWorksheet);
        } else {
            invalidParams();
        }
    }
    
    private void setFormat(Cell cell, String nameFormat) {
		String[] format = nameFormat.split("!");
		Formateable formateable;
		switch(format[0]) {
		case "N":
			formateable = ((Formateable)cell.getExpression());
			formateable.setFormat(new FormatNumericConDecimales(Integer.parseInt(format[1])));
			break;
		case "M":
			formateable = ((Formateable)cell.getExpression());
			formateable.setFormat(new FormatMoneyConDecimales(Integer.parseInt(format[1])));
			break;
		case "DMA":
			formateable = ((Formateable)cell.getExpression());
			formateable.setFormat(new FormatDateDMA());
			break;
		case "AMD":
			formateable = ((Formateable)cell.getExpression());
			formateable.setFormat(new FormatDateAMD());
			break;	
		case "MDA":
			formateable = ((Formateable)cell.getExpression());
			formateable.setFormat(new FormatDateMDA());
			break;
		default: 
			invalidFormat();
		}
	}

	private void invalidFormat() {
		System.out.println("Formato invalido!");
	}

	private void imports(String[] args) {
	    if (args.length == 3 && args[1].equals("json") ) {
            try {
                this.spreadSheet = this.exporter.importSpreadSheetFromJson("spreadSheet.json");
            } catch (IOException e) {
                System.out.println("Error IO");
            }
        } else if (args.length == 2 && args[1].equals("csv")) {
                System.out.println("Comming soon");            
        } else {
            invalidParams();
        }
        
    }

    private void export(String[] args) {
        if (args.length == 3) {
            WorkSheet ws = this.spreadSheet.getWorksheetByName(args[2]); 
            try {
                this.exporter.exportWorkSheetToCSV(ws, args[2] + ".csv");
            } catch (IOException e) {
                System.out.println("Error IO");
            }
        } else if (args.length == 2 && args[1].equals("json")) {
            try {
                this.exporter.exportSpreadSheetToJson(this.spreadSheet, "spreadSheet.json");
            } catch (IOException e) {
                System.out.println("Error IO");
            }
        } else {
            invalidParams();
        }
        for (WorkSheet ws: this.spreadSheet.getWorksheets()) {
            drawSheet(ws.getName());
        }
    }

    private void undo() {
    	// Aca recupero el HistoricalElemente del deshacer
    	HistoricalElement historicalElement=  this.spreadSheet.getHistorical().getUndoHistoricalElement();
        String nameCell= historicalElement.getNameCell();
        String nameWorsheet= historicalElement.getNameWorksheet();
        
        Cell cell= this.spreadSheet.getWorksheetByName(nameWorsheet).getCellByName(nameCell);
        cell.setExpression(historicalElement.getExpression());
    }

    private void redo() {
    	// Aca recupero el HistoricalElemente del deshacer
    	HistoricalElement historicalElement=  this.spreadSheet.getHistorical().getRedoHistoricalElement();
        String nameCell= historicalElement.getNameCell();
        String nameWorsheet= historicalElement.getNameWorksheet();
        
        Cell cell= this.spreadSheet.getWorksheetByName(nameWorsheet).getCellByName(nameCell);
        cell.setExpression(historicalElement.getExpression());
        
        
    }
}
