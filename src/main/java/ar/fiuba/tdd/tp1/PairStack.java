package ar.fiuba.tdd.tp1;

import java.util.Stack;

public class PairStack<T> {
    private Stack<T> hacer = new Stack<T>();
    private Stack<T> deshacer = new Stack<T>();

    public T hacer() {

        T elementoPop = null;

        if (!deshacer.empty()) {
            elementoPop = deshacer.pop();
            hacer.push(elementoPop);
        }
        return hacer.peek();
    }

    public T deshacer() {

        T elementoPop = null;

        if (!hacer.empty()) {
            elementoPop = hacer.pop();
            deshacer.push(elementoPop);
        }
        return elementoPop;
    }

    public T peek() {

        return hacer.peek();
    }

    public boolean isEmpty() {

        return hacer.isEmpty();
    }

    public void add(T item) {

        hacer.push(item);
        deshacer.clear();
    }
}
