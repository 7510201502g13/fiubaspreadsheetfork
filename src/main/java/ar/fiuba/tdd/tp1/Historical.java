package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.expressions.Expression;

import java.util.HashMap;

public class Historical {
    // TODO Singleton
    private PairStack<String> pair;
    private HashMap<String, PairStack<HistoricalElement>> cellStack;

    public Historical() {
        pair = new PairStack<String>();
        cellStack = new HashMap<String, PairStack<HistoricalElement>>();
    }

    public void addCell(HistoricalElement cell) {

        pair.add(cell.getID());

        if (!cellStack.containsKey(cell.getID())) {

            cellStack.put(cell.getID(), new PairStack<HistoricalElement>());
            cellStack.get(cell.getID()).add(cell);

        } else {
            cellStack.get(cell.getID()).add(cell);
        }
    }

    public String hacer() {
        String nameCell = pair.hacer();
        cellStack.get(nameCell).hacer();
        return nameCell;
    }

    public String deshacer() {
        String nameId = pair.deshacer();
        cellStack.get(nameId).deshacer();
        return nameId;
    }

    public Expression restoreValue(String nameWorksheetAndCell) {
        Expression result = null;

        if (!cellStack.get(nameWorksheetAndCell).isEmpty()) {
            result = cellStack.get(nameWorksheetAndCell).peek().getExpression();
        }
        return result;
    }

	public HistoricalElement getUndoHistoricalElement() {
		HistoricalElement result=null;
		String nameWorksheetAndCell= deshacer();
        if (!cellStack.get(nameWorksheetAndCell).isEmpty()) {
            result = cellStack.get(nameWorksheetAndCell).deshacer();
        }
        return result;
	}
	
	
	
	public HistoricalElement getRedoHistoricalElement() {
		HistoricalElement result=null;
		String nameWorksheetAndCell= hacer();
        if (!cellStack.get(nameWorksheetAndCell).isEmpty()) {
            result = cellStack.get(nameWorksheetAndCell).hacer();
        }
        return result;
	}
}
