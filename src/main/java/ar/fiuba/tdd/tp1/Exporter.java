package ar.fiuba.tdd.tp1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.StringValueExpression;

public class Exporter {

	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";

	public Exporter() {
	}

	private void exportObjectToJson(Object obj, String path) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
		writer.writeValue(new File(path), obj);
	}

	void exportWorkSheetToJson(WorkSheet work, String path) throws IOException {
		exportObjectToJson(work, path);
	}

	WorkSheet importWorkSheetFromJson(String path) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(new File(path), WorkSheet.class);
	}

	SpreadSheet importSpreadSheetFromJson(String path) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(new File(path), SpreadSheet.class);
	}

	void exportSpreadSheetToJson(SpreadSheet sp, String path) throws IOException {
		exportObjectToJson(sp, path);
	}

	public void exportWorkSheetToCSV(WorkSheet work, String path) throws IOException {

		String nameCell = getMaxNameCell(work.getCellNames());

		Matrix<String> tableCsv = new Matrix<String>(convertCellNameToRow(nameCell), convertCellNameToColl(nameCell));

		for (String ncell : work.getCellNames()) {
			tableCsv.set(convertCellNameToRow(ncell), convertCellNameToColl(ncell),
					work.getCellByName(ncell).evaluate().toString());
		}
		writeTableCsvToFile(tableCsv, path);
	}

	public WorkSheet importWorkSheetFromCSV(String path) throws IOException {

		Path p = Paths.get(path);
		String nameArch = p.getFileName().toString();
		WorkSheet work = WorkSheet.createWorkSheet(nameArch.substring(0, nameArch.lastIndexOf('.')));

		BufferedReader bufRdr = new BufferedReader(new FileReader(path));

		String line = null;

		int row = 1;

		while ((line = bufRdr.readLine()) != null) {

			String[] vecDataCell = line.split(COMMA_DELIMITER);
			for (int col = 0; col < vecDataCell.length; col++) {
				String dataString = vecDataCell[col];
				if (!dataString.isEmpty()) {

					Cell cell = work.addNewCell(convertMatrixPosToCellName(row, col + 1));
					if (isNumber(dataString)) {
						cell.setExpression(new NumericValueExpression(Double.parseDouble(dataString)));
					} else {
						cell.setExpression(new StringValueExpression(dataString));
					}

				}
			}
			row++;
		}
		bufRdr.close();
		return work;
	}

	private boolean isNumber(String data) {
		try {
			Double number = Double.parseDouble(data);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private String convertMatrixPosToCellName(int i, int j) {
		return (convertNumToCellName(i) + j);

	}

	private String convertNumToCellName(int number) {
		StringBuilder sb = new StringBuilder();
		while (number-- > 0) {
			sb.append((char) ('A' + (number % 26)));
			number /= 26;
		}
		return sb.reverse().toString();
	}

	private int convertCellNameToColl(String nameCell) {
		String colString = nameCell.replaceAll("[^0-9]", "");
		return Integer.parseInt(colString);

	}

	private int convertCellNameToRow(String nameCell) {
		String rowString = nameCell.replaceAll("[0-9]", "");
		int row = 0;
		for (int i = 0; i < rowString.length(); i++) {
			row = row * 26 + (rowString.charAt(i) - ('A' - 1));
		}

		return row;

	}

	private void writeTableCsvToFile(Matrix<String> tableCsv, String path) throws IOException {
		FileWriter fileWriter = new FileWriter(path);

		for (int i = 1; i <= tableCsv.getNumRow(); i++) {
			for (int j = 1; j <= tableCsv.getNumCol(); j++) {
				if (tableCsv.get(i, j) != null) {
					fileWriter.append(String.valueOf((tableCsv.get(i, j))));
				}

				if (j < tableCsv.getNumCol()) {
					fileWriter.append(COMMA_DELIMITER);
				}
			}
			fileWriter.append(NEW_LINE_SEPARATOR);
		}
		fileWriter.close();
	}

	private String getMaxNameCell(List<String> listNameCells) {

		Collections.sort(listNameCells, Collections.reverseOrder());
		return listNameCells.get(0);
	}
}
