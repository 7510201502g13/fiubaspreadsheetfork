package ar.fiuba.tdd.tp1;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import ar.fiuba.tdd.tp1.expressions.ArithmeticExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;

public class ExporterTest {
	private static final double DELTA = 0.00001;

	Exporter exporter = new Exporter();

	@Test
	public void exportWorkSheetToCSV() throws IOException {

		WorkSheet work1 = WorkSheet.createWorkSheet("hoja1");

		String nameFile = work1.getName() + ".csv";

		work1.addNewCell("A1");
		work1.addNewCell("A2");
		work1.addNewCell("A3");
		work1.addNewCell("A4");
		work1.addNewCell("C10");

		work1.getCellByName("A1").setExpression(new NumericValueExpression(2.0));
		work1.getCellByName("A2").setExpression(new NumericValueExpression(2.5));
		work1.getCellByName("A4").setExpression(new NumericValueExpression(8.0));
		work1.getCellByName("C10").setExpression(new NumericValueExpression(13.0));

		work1.getCellByName("A3")
				.setExpression(ArithmeticExpression.create(work1.getCellByName("A1"), work1.getCellByName("A2"), '+'));

		exporter.exportWorkSheetToCSV(work1, nameFile);

		Cell cellIN = exporter.importWorkSheetFromCSV(nameFile).getCellByName("C10");

		assertEquals("hoja1", (exporter.importWorkSheetFromCSV(nameFile).getName()));

		assertEquals(13, ((NumericValueExpression) cellIN.evaluate()).getValor(), DELTA);

	}

	@Test
	public void exportWorkSheet() throws IOException {
		WorkSheet work1 = WorkSheet.createWorkSheet("hoja1");

		String nameFile = work1.getName() + ".json";

		work1.addNewCell("A1");
		work1.addNewCell("A2");
		work1.addNewCell("A3");
		work1.addNewCell("A4");
		work1.addNewCell("A5");

		work1.getCellByName("A1").setExpression(new NumericValueExpression(2.0));
		work1.getCellByName("A2").setExpression(new NumericValueExpression(2.5));

		work1.getCellByName("A3")
				.setExpression(ArithmeticExpression.create(work1.getCellByName("A1"), work1.getCellByName("A2"), '+'));

		exporter.exportWorkSheetToJson(work1, nameFile);

		Cell result = exporter.importWorkSheetFromJson(nameFile).getCellByName("A3");

		assertEquals(4.5, ((NumericValueExpression) result.evaluate()).getValor(), DELTA);
	}

	@Test
	public void exportSpreadSheet() throws IOException {

		SpreadSheet sheet = SpreadSheet.createSpreadsheet("book");
		Parser parser = new Parser(sheet);

		String nameFile = sheet.getName() + ".json";

		sheet.addNewWorksheet("Hoja1");
		sheet.addNewWorksheet("Hoja2");

		Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
		cell.setExpression(new NumericValueExpression(8.0));
		Cell cell2 = sheet.getWorksheetByName("Hoja2").addNewCell("A1");
		cell2.setExpression(new NumericValueExpression(7.0));
		Cell cell3 = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
		cell3.setExpression(parser.parse("=Hoja1!A1+Hoja2!A1"));

		exporter.exportSpreadSheetToJson(sheet, nameFile);

		SpreadSheet sheetIN = exporter.importSpreadSheetFromJson(nameFile);

		Cell cell3IN = sheetIN.getWorksheetByName("Hoja1").getCellByName("A2");

		assertEquals(15.0, ((NumericValueExpression) cell3IN.evaluate()).getValor(), DELTA);

	}

}
