package ar.fiuba.tdd.tp1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import ar.fiuba.tdd.tp1.expressions.ArithmeticExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;

public class CellTest {

    private static final double DELTA = 0.00001;
    private SpreadSheet spreadSheet = null;

    public Cell createCell() {
        return new Cell();
    }

    @Before
    public void setUp() {
        spreadSheet = SpreadSheet.createSpreadsheet("myBook");
        spreadSheet.addNewWorksheet("hoja1");
        spreadSheet.addNewWorksheet("hoja2");
    }

    @Test(expected = CellAlreadyExistException.class)
    public void addExistentCellIntoTheSameSheet() {
        WorkSheet hoja1 = spreadSheet.getWorksheetByName("hoja1");
        hoja1.addNewCell("A1");
        hoja1.addNewCell("A2");
        hoja1.addNewCell("A1");
    }

    @Test
    public void test() {

        Cell c1 = createCell();
        Cell c2 = createCell();
        Cell c3 = createCell();
        Cell c4 = createCell();
        Cell c5 = createCell();

        ArithmeticExpression add1 = ArithmeticExpression.create(c1, c3, '+'); // 5+7=12
        ArithmeticExpression add2 = ArithmeticExpression.create(c2, c4, '+');// 6+12=18

        c4.setExpression(add1);
        c5.setExpression(add2);
        c1.setExpression(new NumericValueExpression(5.0));
        c2.setExpression(new NumericValueExpression(6.0));
        c3.setExpression(new NumericValueExpression(7.0));
        assertEquals(18.0, ((NumericValueExpression) c5.evaluate()).getValor(),
                DELTA);
    }

    @Test
    public void sumaCellValues() {

        WorkSheet work1 = spreadSheet.getWorksheetByName("hoja1");

        work1.addNewCell("A1");
        work1.addNewCell("A2");
        work1.addNewCell("A3");
        work1.addNewCell("A4");
        work1.addNewCell("A5");

        work1.getCellByName("A1")
                .setExpression(new NumericValueExpression(2.0));
        work1.getCellByName("A2")
                .setExpression(new NumericValueExpression(2.5));

        work1.getCellByName("A3").setExpression(
                ArithmeticExpression.create(work1.getCellByName("A1"),
                        work1.getCellByName("A2"), '+'));

        Cell result = work1.getCellByName("A3");
        assertEquals(4.5,
                ((NumericValueExpression) result.evaluate()).getValor(), DELTA);
    }

    @Test
    public void nestedExpressionsTest() {
        SpreadSheet spreadsheet = SpreadSheet.createSpreadsheet("myBook2");
        spreadsheet.addNewWorksheet("hoja1");
        spreadsheet.addNewWorksheet("hoja2");

        WorkSheet work1 = spreadsheet.getWorksheetByName("hoja1");

        work1.addNewCell("A1");
        work1.addNewCell("A2");
        work1.addNewCell("A3");
        work1.addNewCell("A4");
        work1.addNewCell("A5");

        work1.getCellByName("A1")
                .setExpression(new NumericValueExpression(2.0));
        work1.getCellByName("A2")
                .setExpression(new NumericValueExpression(2.5));

        work1.getCellByName("A3").setExpression(
                ArithmeticExpression.create(work1.getCellByName("A1"),
                        work1.getCellByName("A2"), '+')); // 4.5
        work1.getCellByName("A4").setExpression(
                ArithmeticExpression.create(work1.getCellByName("A1"),
                        work1.getCellByName("A2"), '-')); // -0.5
        work1.getCellByName("A5").setExpression(
                ArithmeticExpression.create(work1.getCellByName("A3"),
                        work1.getCellByName("A4"), '+')); // 4

        Cell result = work1.getCellByName("A5");
        assertEquals(4,
                ((NumericValueExpression) result.evaluate()).getValor(), DELTA);
    }
}
