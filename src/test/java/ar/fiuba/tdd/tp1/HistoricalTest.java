package ar.fiuba.tdd.tp1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp1.expressions.ArithmeticExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;

public class HistoricalTest {
    private static final double DELTA = 0.00001;

    public Cell createCell() {
        return new Cell();
    }

    @Test
    public void deshacerTest() {

        Historical historical = new Historical();
        Cell c1 = createCell();// A1

        c1.setExpression(new NumericValueExpression(5.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));

        c1.setExpression(new NumericValueExpression(10.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));

        String nameCell = historical.deshacer();
        c1.setExpression(historical.restoreValue(nameCell));

        assertEquals(5.0, ((NumericValueExpression) c1.evaluate()).getValor(),
                DELTA);
    }

    @Test
    public void hacerTest() {

        Historical historical = new Historical();
        Cell c1 = createCell();// A1

        c1.setExpression(new NumericValueExpression(5.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));

        c1.setExpression(new NumericValueExpression(10.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));

        historical.deshacer();

        String nameCell = historical.hacer();
        c1.setExpression(historical.restoreValue(nameCell));

        assertEquals(10.0, ((NumericValueExpression) c1.evaluate()).getValor(),
                DELTA);
    }

    @Test
    public void deshacerExpressionTest() {

        Historical historical = new Historical();

        Cell c1 = createCell();// A1
        c1.setExpression(new NumericValueExpression(5.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));
        Cell c2 = createCell();// A2
        c2.setExpression(new NumericValueExpression(10.0));
        historical.addCell(new HistoricalElement("W1", "A2", c2));
        Cell c3 = createCell();// B1
        c3.setExpression(ArithmeticExpression.create(c1, c2, '-'));
        historical.addCell(new HistoricalElement("W1", "B1", c3));

        c3.setExpression(ArithmeticExpression.create(c1, c2, '+'));
        historical.addCell(new HistoricalElement("W1", "B1", c3));

        String nameCell = historical.deshacer();
        c3.setExpression(historical.restoreValue(nameCell));

        assertEquals(-5, ((NumericValueExpression) c3.evaluate()).getValor(),
                DELTA);
    }

    @Test(expected = NullPointerException.class)
    public void deshacerTodoTest() {

        Historical historical = new Historical();
        Cell c1 = createCell();// A1

        c1.setExpression(new NumericValueExpression(5.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));

        c1.setExpression(new NumericValueExpression(10.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));

        historical.deshacer();
        String name = historical.deshacer();

        c1.setExpression(historical.restoreValue(name));
        c1.evaluate();
    }

    @Test
    public void deshacerTestHistorialElement() {

        Historical historical = new Historical();
        Cell c1 = createCell();// A1

        Cell c2 = createCell();// A1

        c1.setExpression(new NumericValueExpression(5.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));

        c2.setExpression(new NumericValueExpression(10.0));
        historical.addCell(new HistoricalElement("W2", "A1", c2));

        c1.setExpression(new NumericValueExpression(3.0));
        historical.addCell(new HistoricalElement("W1", "A1", c1));

        c2.setExpression(new NumericValueExpression(4.0));
        historical.addCell(new HistoricalElement("W2", "A1", c2));

        String nameIdHistoricalElement = historical.deshacer();
        c1.setExpression(historical.restoreValue(nameIdHistoricalElement));

        assertEquals(10.0, ((NumericValueExpression) c1.evaluate()).getValor(),
                DELTA);

        String nameIdHistoricalElement2 = historical.deshacer();
        c1.setExpression(historical.restoreValue(nameIdHistoricalElement2));

        assertEquals(5.0, ((NumericValueExpression) c1.evaluate()).getValor(),
                DELTA);
    }
}
