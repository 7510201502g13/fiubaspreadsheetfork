package ar.fiuba.tdd.tp1.format;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import ar.fiuba.tdd.tp1.expressions.formateable.DateValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.format.date.FormatDateAMD;
import ar.fiuba.tdd.tp1.format.date.FormatDateDMA;
import ar.fiuba.tdd.tp1.format.date.FormatDateMDA;
import ar.fiuba.tdd.tp1.format.money.FormatMoneyConDecimales;
import ar.fiuba.tdd.tp1.format.numeric.FormatNumericConDecimales;
import ar.fiuba.tdd.tp1.format.numeric.FormatNumericSinDecimales;


public class DateFormatTest {

	@Test
	public void simpleDateTest() {

		DateValueExpression stringValue = new DateValueExpression(LocalDate.of(2105, 1, 1));

		stringValue.setFormat(new FormatDateAMD());

	}

	@Test
	public void simpleDateTestDMA() {

		/**
		 * La clase formatter hace como de impresora de los formateables a
		 * travez del metodo format Para ello las clases formateables tendranq
		 * ue ya tener seteado un formato a traves del metoso setFormat Eso hace
		 * que no tenga que cambiar el estado del objeto sino que simplemente el
		 * formato elegido servira para mostrarlo como se espera que se muestre
		 */
		DateValueExpression dateExpression = new DateValueExpression(LocalDate.of(2016, 2, 29));
		dateExpression.setFormat(new FormatDateDMA());

		assertEquals("29-02-2016", Formatter.format(dateExpression));

	}

	@Test
	public void simpleDateTestMDA() {

		DateValueExpression dateExpression = new DateValueExpression(LocalDate.of(2015, 6, 30));
		dateExpression.setFormat(new FormatDateMDA());

		assertEquals("06-30-2015", dateExpression.toString());

	}
	
	

	@Test
	public void moneyConDecimalesTest() {

		NumericValueExpression moneyExp = new NumericValueExpression(10.78936388);
		moneyExp.setFormat(new FormatMoneyConDecimales(4));

		assertEquals("$ 10.7894", moneyExp.toString());

	}
	
	
	@Test
	public void numericConDecimalesTest() {

		NumericValueExpression num = new NumericValueExpression(10.78936388);
		num.setFormat(new FormatNumericConDecimales(3));

		assertEquals("10.789", num.toString());

	}

	   @Test
	    public void numericSinDecimales() {

	        NumericValueExpression num = new NumericValueExpression(10.78936388);
	        num.setFormat(new FormatNumericSinDecimales());

	        assertEquals("11", num.toString());

	    }
	

}
