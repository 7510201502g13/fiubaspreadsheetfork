package ar.fiuba.tdd.tp1.acceptance.driver;

import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.HistoricalElement;
import ar.fiuba.tdd.tp1.Parser;
import ar.fiuba.tdd.tp1.SpreadSheet;
import ar.fiuba.tdd.tp1.WorkSheet;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.StringValueExpression;

public class TestDriver implements SpreadSheetTestDriver {

    private List<SpreadSheet> spreadSheets = new ArrayList<SpreadSheet>();
    private Parser parser;
    
    @Override
    public List<String> workBooksNames() {
        List<String> workBookNames = new ArrayList<String>();
        for (SpreadSheet s: spreadSheets) {
            workBookNames.add(s.getName());
        } 
        return workBookNames;
    }

    @Override
    public void createNewWorkBookNamed(String name) {
        SpreadSheet sp = SpreadSheet.createSpreadsheet(name);
        spreadSheets.add(sp);
        WorkSheet ws = WorkSheet.createWorkSheet("default");
        sp.setDefaultWS(ws);
        sp.addExternalWorkSheet(ws);
    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {
        SpreadSheet book = spreadSheets.get(spreadSheets.indexOf(SpreadSheet.createSpreadsheet(workbookName)));
        book.addNewWorksheet(name);
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {
        SpreadSheet book = spreadSheets.get(spreadSheets.indexOf(SpreadSheet.createSpreadsheet(workBookName)));
        List<WorkSheet> ws = book.getWorkSheets();
        List<String> wsNames = new ArrayList<String>();
        for (WorkSheet w: ws) {
            wsNames.add(w.getName());
        }
        return wsNames;
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName,
            String cellId, String value) {
        SpreadSheet book = spreadSheets.get(spreadSheets.indexOf(SpreadSheet.createSpreadsheet(workBookName)));
        WorkSheet ws = book.getWorksheetByName(workSheetName);
        parser = new Parser(book);
        try {
            ws.getCellByName(cellId).setExpression(parser.parse(value));
            book.getHistorical().addCell(new HistoricalElement(ws.getName(), cellId, ws.getCellByName(cellId)));
        } catch (Exception e) {
            throw new BadFormulaException();
        }
        
    }

    @Override
    public String getCellValueAsString(String workBookName,
            String workSheetName, String cellId) {
        SpreadSheet book = spreadSheets.get(spreadSheets.indexOf(SpreadSheet.createSpreadsheet(workBookName)));
        WorkSheet ws = book.getWorksheetByName(workSheetName);
        try {
            return ((StringValueExpression)ws.getCellByName(cellId).evaluate()).getValor();
        } catch (Exception e) {
            throw new BadFormulaException();
        }
       
    }

    @Override
    public double getCellValueAsDouble(String workBookName,
            String workSheetName, String cellId) {
        double result = 0.0;
        SpreadSheet book = spreadSheets.get(spreadSheets.indexOf(SpreadSheet.createSpreadsheet(workBookName)));
        WorkSheet ws = book.getWorksheetByName(workSheetName);
        try {
            result = ((NumericValueExpression)ws.getCellByName(cellId).evaluate()).getValor();
        } catch (Exception exception) {
            throw new BadFormulaException();
        }
        return result;
    }

    @Override
    public void undo() {
      SpreadSheet spreadSheet=   this.spreadSheets.get(0);
      
      HistoricalElement historicalElement=  spreadSheet.getHistorical().getUndoHistoricalElement();
      String nameCell= historicalElement.getNameCell();
      String nameWorsheet= historicalElement.getNameWorksheet();
      
      Cell cell= spreadSheet.getWorksheetByName(nameWorsheet).getCellByName(nameCell);
      cell.setExpression(historicalElement.getExpression());
    }

    @Override
    public void redo() {
    	 SpreadSheet spreadSheet=   this.spreadSheets.get(0);
         
         HistoricalElement historicalElement=  spreadSheet.getHistorical().getRedoHistoricalElement();
         String nameCell= historicalElement.getNameCell();
         String nameWorsheet= historicalElement.getNameWorksheet();
         
         Cell cell= spreadSheet.getWorksheetByName(nameWorsheet).getCellByName(nameCell);
         cell.setExpression(historicalElement.getExpression());
        
    }

}
