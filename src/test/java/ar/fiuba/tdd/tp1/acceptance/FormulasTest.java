package ar.fiuba.tdd.tp1.acceptance;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.TestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.UndeclaredWorkSheetException;

public class FormulasTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new TestDriver();
    }

    @Test
    public void sumLiterals() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1",
                "= 1 + 0 + 3.5 - 1");

        assertEquals(3.5,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void sumLiteralsInDifferentRows() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1");
        testDriver.setCellValue("tecnicas", "default", "B2", "2");
        testDriver.setCellValue("tecnicas", "default", "C3", "= A1 + B2");

        assertEquals(1 + 2,
                testDriver.getCellValueAsDouble("tecnicas", "default", "C3"),
                DELTA);
    }

    @Test
    public void subtractLiterals() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1",
                "= 1 - 0 - 3.5 + 1");

        assertEquals(1 - 3.5 - -1,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferences() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A2", "5");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");
        testDriver.setCellValue("tecnicas", "default", "A1",
                "= 1 + A2 + 0.5 - A3");

        assertEquals(1 + 5 + 0.5 - 2,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferenceToReference() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A3", "3");
        testDriver.setCellValue("tecnicas", "default", "A2", "= 2 + A3");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");

        assertEquals(1 + 2 + 3,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferencesFromOtherSpreadSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");
        testDriver.setCellValue("tecnicas", "other", "A2", "-1");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");
        testDriver.setCellValue("tecnicas", "default", "A1",
                "= 1 + other!A2 + 0.5 - A3");

        assertEquals(1 + -1 + 0.5 - 2,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumber() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumberRetrieveAsString() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");
        testDriver.getCellValueAsString("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithReference() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "Hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithoutEqualSymbol() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1 + 2");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Ignore("Optional expected behavior test")
    @Test(expected = UndeclaredWorkSheetException.class)
    public void undeclaredWorkSheetInvocation() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver
                .getCellValueAsString("tecnicas", "undeclaredWorkSheet", "A1");
    }

}
