package ar.fiuba.tdd.tp1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.StringValueExpression;

public class FunctionsTest {
    private static final double DELTA = 0.00001;
    private final SpreadSheet sheet = SpreadSheet.createSpreadsheet("book");
    private final Parser parser = new Parser(sheet);

    @Before
    public void setUp() {
        sheet.addNewWorksheet("Hoja1");
        sheet.addNewWorksheet("Hoja2");
    }

    @Test
    public void concatStrings() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(parser.parse("=CONCAT(hola,mundo)"));
        assertEquals("holamundo",
                ((StringValueExpression) cell.evaluate()).getValor());
    }

    @Test
    public void concatCells() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(parser.parse("hola"));
        Cell cell2 = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell2.setExpression(parser.parse("mundo"));
        Cell cell3 = sheet.getWorksheetByName("Hoja1").addNewCell("A3");
        cell3.setExpression(parser.parse("=CONCAT(Hoja1!A1,Hoja1!A2)"));

        assertEquals("holamundo",
                ((StringValueExpression) cell3.evaluate()).getValor());
    }

    public void setRange() {
        Cell cell1 = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell1.setExpression(parser.parse("2.0"));
        Cell cell2 = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell2.setExpression(parser.parse("4.5"));
        Cell cell3 = sheet.getWorksheetByName("Hoja1").addNewCell("B1");
        cell3.setExpression(parser.parse("6.0"));
        Cell cell4 = sheet.getWorksheetByName("Hoja1").addNewCell("B2");
        cell4.setExpression(parser.parse("0.0"));
    }

    @Test
    public void average() {
        setRange();
        Cell cell5 = sheet.getWorksheetByName("Hoja1").addNewCell("C1");
        cell5.setExpression(parser.parse("=AVERAGE(Hoja1!A1:Hoja1!B2)"));

        assertEquals((2.0 + 4.5 + 6.0 + 0.0) / 4,
                ((NumericValueExpression) cell5.evaluate()).getValor(), DELTA);
    }

    @Test
    public void max() {
        setRange();
        Cell cell5 = sheet.getWorksheetByName("Hoja1").addNewCell("C1");
        cell5.setExpression(parser.parse("=MAX(Hoja1!A1:Hoja1!B2)"));

        assertEquals(6.0,
                ((NumericValueExpression) cell5.evaluate()).getValor(), DELTA);
    }

    @Test
    public void min() {
        setRange();
        Cell cell5 = sheet.getWorksheetByName("Hoja1").addNewCell("C1");
        cell5.setExpression(parser.parse("=MIN(Hoja1!A1:Hoja1!B2)"));

        assertEquals(0.0,
                ((NumericValueExpression) cell5.evaluate()).getValor(), DELTA);
    }

    // @Test
    // public void mixFunctions() {
    // setRange();
    // Cell cell5 = sheet.getWorksheetByName("Hoja1").addNewCell("C1");
    // cell5.setExpression(parser.parse("=CONCAT(MIN(Hoja1!A1:Hoja1!B2),MAX(Hoja1!A1:Hoja1!B2))"));
    // assertEquals("0.06.0",((StringValueExpression)cell5.evaluate()).getValor());
    // }
    //
}
