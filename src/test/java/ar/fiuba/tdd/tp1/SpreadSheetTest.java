package ar.fiuba.tdd.tp1;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Before;
import org.junit.Test;

import ar.fiuba.tdd.tp1.expressions.formateable.DateValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.NumericValueExpression;
import ar.fiuba.tdd.tp1.expressions.formateable.StringValueExpression;

public class SpreadSheetTest {
    private static final double DELTA = 0.00001;
    private final SpreadSheet sheet = SpreadSheet.createSpreadsheet("book");
    private final Parser parser = new Parser(sheet);

    @Before
    public void setUp() {
        sheet.addNewWorksheet("Hoja1");
        sheet.addNewWorksheet("Hoja2");
        sheet.addNewWorksheet("Hoja3");
    }

    @Test(expected = SheetAlreadyExistsException.class)
    public void addExistentSheet() {
        sheet.addNewWorksheet("Hoja1");
    }

    @Test(expected = SheetDoesNotExistException.class)
    public void getInexistentSheet() {
        sheet.getWorksheetByName("NotExists");
    }

    @Test
    public void simpleValue() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(new NumericValueExpression(4.0));
        assertEquals(4.0,
                ((NumericValueExpression) cell.evaluate()).getValor(), DELTA);
    }

    @Test
    public void simpleString() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(new StringValueExpression("Hola Mundo!"));
        assertEquals("Hola Mundo!",
                ((StringValueExpression) cell.evaluate()).getValor());
    }

    @Test
    public void simpleDate() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(new DateValueExpression(LocalDate.of(2015, 10, 17)));
        assertEquals(2015, ((DateValueExpression) cell.evaluate()).getValor()
                .getYear());
        assertEquals(Month.OCTOBER, ((DateValueExpression) cell.evaluate())
                .getValor().getMonth());
        assertEquals(17, ((DateValueExpression) cell.evaluate()).getValor()
                .getDayOfMonth());
    }

    @Test
    public void negativeSimpleValue() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell.setExpression(new NumericValueExpression(-4.0));
        assertEquals(-4.0,
                ((NumericValueExpression) cell.evaluate()).getValor(), DELTA);
    }

    @Test
    public void addSimpleValues() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("B1");
        cell.setExpression(parser.parse("=4+1"));
        assertEquals(5.0,
                ((NumericValueExpression) cell.evaluate()).getValor(), DELTA);
    }

    @Test
    public void subtractSimpleValues() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(parser.parse("=4-1"));
        assertEquals(3.0,
                ((NumericValueExpression) cell.evaluate()).getValor(), DELTA);
    }

    @Test
    public void addAndSubtractSimpleValues() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell.setExpression(parser.parse("=4-1+3"));
        assertEquals(6.0,
                ((NumericValueExpression) cell.evaluate()).getValor(), DELTA);
    }

    @Test
    public void multipleAddSimpleValues() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell.setExpression(parser.parse("=4+1+3"));
        assertEquals(8.0,
                ((NumericValueExpression) cell.evaluate()).getValor(), DELTA);
    }

    @Test
    public void multipleSubtractSimpleValues() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell.setExpression(parser.parse("=-4-1-10"));
        assertEquals(-15.0,
                ((NumericValueExpression) cell.evaluate()).getValor(), DELTA);
    }

    @Test
    public void multipleAddAndSubtractSimpleValues() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell.setExpression(parser.parse("=-4+1+10-4-3"));
        assertEquals(0.0,
                ((NumericValueExpression) cell.evaluate()).getValor(), DELTA);
    }

    @Test
    public void cellReference() {
        WorkSheet ws = sheet.getWorksheetByName("Hoja1");
        Cell cell = ws.addNewCell("A1");
        cell.setExpression(new NumericValueExpression(8.0));
        Cell cell2 = ws.addNewCell("A2");
        cell2.setExpression(parser.parse("=Hoja1!A1"));
        assertEquals(8.0,
                ((NumericValueExpression) cell2.evaluate()).getValor(), DELTA);
    }

    @Test
    public void negativeReference() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("C1");
        cell.setExpression(new NumericValueExpression(1.0));
        Cell cell2 = sheet.getWorksheetByName("Hoja1").addNewCell("A32");
        cell2.setExpression(parser.parse("=-Hoja1!C1"));
        assertEquals(-1.0,
                ((NumericValueExpression) cell2.evaluate()).getValor(), DELTA);
    }

    @Test
    public void addCells() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(new NumericValueExpression(8.0));
        Cell cell2 = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell2.setExpression(new NumericValueExpression(7.0));
        Cell cell3 = sheet.getWorksheetByName("Hoja1").addNewCell("A3");
        cell3.setExpression(parser.parse("=Hoja1!A1+Hoja1!A2"));
        assertEquals(15.0,
                ((NumericValueExpression) cell3.evaluate()).getValor(), DELTA);
    }

    @Test
    public void subtractCells() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(new NumericValueExpression(8.0));
        Cell cell2 = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell2.setExpression(new NumericValueExpression(7.0));
        Cell cell3 = sheet.getWorksheetByName("Hoja1").addNewCell("A3");
        cell3.setExpression(parser.parse("=Hoja1!A1-Hoja1!A2"));
        assertEquals(1.0,
                ((NumericValueExpression) cell3.evaluate()).getValor(), DELTA);
    }

    @Test
    public void referenceBetweenSheets() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(new NumericValueExpression(8.0));
        Cell cell2 = sheet.getWorksheetByName("Hoja2").addNewCell("A1");
        cell2.setExpression(new NumericValueExpression(7.0));
        Cell cell3 = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell3.setExpression(parser.parse("=Hoja1!A1+Hoja2!A1"));
        assertEquals(15.0,
                ((NumericValueExpression) cell3.evaluate()).getValor(), DELTA);
    }

    @Test
    public void complexCalc() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("A1");
        cell.setExpression(new NumericValueExpression(8.0));
        Cell cell2 = sheet.getWorksheetByName("Hoja2").addNewCell("A1");
        cell2.setExpression(new NumericValueExpression(7.0));
        Cell cell3 = sheet.getWorksheetByName("Hoja1").addNewCell("A2");
        cell3.setExpression(parser.parse("=Hoja1!A1+Hoja2!A1"));
        Cell cell4 = sheet.getWorksheetByName("Hoja3").addNewCell("B1");
        cell4.setExpression(parser.parse("=Hoja1!A2+4-0+0-2"));
        assertEquals(15.0,
                ((NumericValueExpression) cell3.evaluate()).getValor(), DELTA);
        assertEquals(17.0,
                ((NumericValueExpression) cell4.evaluate()).getValor(), DELTA);
    }

    @Test
    public void complexCalc2() {
        Cell cell = sheet.getWorksheetByName("Hoja1").addNewCell("C43");
        cell.setExpression(new NumericValueExpression(-3.0));
        Cell cell2 = sheet.getWorksheetByName("Hoja1").addNewCell("C44");
        cell2.setExpression(parser.parse("=-Hoja1!C43-Hoja1!C43"));
        Cell cell3 = sheet.getWorksheetByName("Hoja1").addNewCell("C45");
        cell3.setExpression(new NumericValueExpression(-7.0));
        Cell cell4 = sheet.getWorksheetByName("Hoja2").addNewCell("B12");
        cell4.setExpression(parser.parse("=-Hoja1!C43+3+Hoja1!C44-4-Hoja1!C45"));
        assertEquals(15.0,
                ((NumericValueExpression) cell4.evaluate()).getValor(), DELTA);
    }
}